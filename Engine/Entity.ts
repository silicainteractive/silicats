import SilMath = require("SilMath/Vector2");

export class Entity {
    public position: SilMath.Vector2;
    public size: SilMath.Vector2;
    
    constructor(position: SilMath.Vector2, size: SilMath.Vector2) {
        this.position = position;
        this.size = size;
           
    }
    public OnClick(event: MouseEvent) :void {
        
    }
    public ContainsPoint(point: SilMath.Vector2): boolean {
        var test1 = point.x >= this.position.x && point.x <= this.position.x + this.size.x;
        var test2 = point.y >= this.position.y && point.y <= this.position.y + this.size.y;
        return test1 && test2;
    }
    public Update(): void { throw new Error("Abstract!"); }
    public Draw(context: CanvasRenderingContext2D): void { throw new Error("Abstract!"); }
}
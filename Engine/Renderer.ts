export class Renderer {
    public context: CanvasRenderingContext2D;
    public canvas: HTMLCanvasElement;
    public ui: HTMLDivElement;
    constructor(public container: HTMLElement, public width: number, public height: number) {
        this.canvas = document.createElement("canvas");
        this.context = this.canvas.getContext("2d");
        this.canvas.width = width;
        this.canvas.height = height;
        this.canvas.style.position = "absolute";
        container.appendChild(this.canvas);
    }
    public DebugDraw(fps: number) {
        this.context.font = "16px Arial";
        this.context.fillText(fps.toPrecision(2).toString(), 10, 15);
        console.log(fps);
    }
    public Clear() {
        this.context.clearRect(0, 0, this.width, this.height);
    }
} 
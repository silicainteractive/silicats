import SilMath = require("silicats/SilMath/Vector2");

export class Input {
    private static instance: Input;
    constructor(public canvas: HTMLCanvasElement) {

    }
    public static Initialize(canvas: HTMLCanvasElement) {
        Input.instance = new Input(canvas);
    }
    public static GetMousePos(event: MouseEvent): SilMath.Vector2 {
        var x = event.x - Input.instance.canvas.offsetLeft + window.pageXOffset;
        var y = event.y - Input.instance.canvas.offsetTop + window.pageYOffset;
        return new SilMath.Vector2(x, y);
    }
} 
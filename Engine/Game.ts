import Engine = require("silicats/Engine/State");
import Renderer = require("silicats/Engine/Renderer");
import Input = require("Input");
import UI = require("UI/Frame");

export class Game {
    public renderer: Renderer.Renderer;
    public currentState: Engine.State;
    public isActive: boolean;
    public fps: number;
    public currentFPS: number = 0;
    private startTime: Date; 
    public gametime: Date;
    public input: Input.Input;
    public UI: UI.Frame;
    public stepDraw: boolean;
    public debug: boolean;
    constructor(public container: HTMLElement, startingState: Engine.State) {
        this.renderer = new Renderer.Renderer(container, container.offsetWidth, container.offsetHeight);
        this.UI = new UI.Frame(container, container.offsetWidth, container.offsetHeight);
        this.SwitchState(startingState);
        this.isActive = true;
        this.fps = 15;
        this.gametime = this.startTime = new Date();
        Input.Input.Initialize(this.renderer.canvas);
    }
    public Start() {
        setInterval(this.Update.bind(this), 1000 / this.fps);
    }
    public Scale(s: number) {
        this.renderer.context.scale(s,s);
    }
    public SwitchState(newState: Engine.State) {
        this.currentState = newState;
        this.renderer.canvas.onclick = this.currentState.OnClick.bind(this.currentState);
    }
    public lastTime = new Date();
    public Update() {
        if (!this.stepDraw) {
            this.Draw();
            this.currentState.Update();
        }
        if (this.debug) {
            this.gametime = new Date();
            this.currentFPS = 1000 / (this.gametime.getMilliseconds() - this.lastTime.getMilliseconds());
            this.lastTime = this.gametime;
        }
    }
    public Draw() {
        this.renderer.Clear()
        //this.renderer.DebugDraw(this.currentFPS);
        this.currentState.Draw(this.renderer.context);
        
    }
}


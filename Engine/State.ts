import Engine = require("Entity");

export class State {
    public children: Array<Engine.Entity>;
    constructor() {
        this.children = new Array<Engine.Entity>();
    }
    public AddChild(child: Engine.Entity) {
        this.children.push(child);
    }
    public RemoveChild(child: Engine.Entity) {
        var m = this.children.lastIndexOf(child);
        delete this.children[m];
    }
    public Create() {
        throw new Error("ABSTRACT");
    }
    public Update() {
        this.children.forEach((c) => {
            c.Update();
        });
    }
    public OnClick(event: MouseEvent) {
        this.children.forEach((c) => {
            c.OnClick(event);
        });
    }
    public Draw(context: CanvasRenderingContext2D) {
        this.children.forEach((c) => {
            c.Draw(context);
        });
    }
}
define(["require", "exports", "silicats/Engine/Renderer", "Input", "UI/Frame"], function(require, exports, Renderer, Input, UI) {
    var Game = (function () {
        function Game(container, startingState) {
            this.container = container;
            this.currentFPS = 0;
            this.lastTime = new Date();
            this.renderer = new Renderer.Renderer(container, container.offsetWidth, container.offsetHeight);
            this.UI = new UI.Frame(container, container.offsetWidth, container.offsetHeight);
            this.SwitchState(startingState);
            this.isActive = true;
            this.fps = 15;
            this.gametime = this.startTime = new Date();
            Input.Input.Initialize(this.renderer.canvas);
        }
        Game.prototype.Start = function () {
            setInterval(this.Update.bind(this), 1000 / this.fps);
        };
        Game.prototype.Scale = function (s) {
            this.renderer.context.scale(s, s);
        };
        Game.prototype.SwitchState = function (newState) {
            this.currentState = newState;
            this.renderer.canvas.onclick = this.currentState.OnClick.bind(this.currentState);
        };

        Game.prototype.Update = function () {
            if (!this.stepDraw) {
                this.Draw();
                this.currentState.Update();
            }
            if (this.debug) {
                this.gametime = new Date();
                this.currentFPS = 1000 / (this.gametime.getMilliseconds() - this.lastTime.getMilliseconds());
                this.lastTime = this.gametime;
            }
        };
        Game.prototype.Draw = function () {
            this.renderer.Clear();

            this.currentState.Draw(this.renderer.context);
        };
        return Game;
    })();
    exports.Game = Game;
});

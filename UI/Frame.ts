import SilMath = require("SilMath/MathHelper");

export class Frame {
    public ui: HTMLDivElement;
    public open: boolean;
    private collapseButton: HTMLButtonElement;
    private openPosition: number;
    private closedPostion: number;
    private animID: number;
    constructor(public container: HTMLElement, public width: number, public height: number) {
        this.ui = document.createElement("div");
        this.ui.className = "UIFrame";
        container.appendChild(this.ui);
        this.open = true;
        var collapseButton = this.collapseButton = document.createElement("button");
        collapseButton.className = "UIElement";
        collapseButton.style.position = "absolute";
        collapseButton.style.width = "100%";
        collapseButton.style.height = (height / 25).toString() + "px";
        collapseButton.style.bottom = "0";
        collapseButton.textContent = "Close UI";
        this.ui.appendChild(collapseButton);
        collapseButton.onclick = this.OnClick.bind(this);
        this.openPosition = this.ui.offsetTop;
        this.closedPostion = this.openPosition - collapseButton.offsetTop;
    }

    public AddButton(text:string, callback: (ev: MouseEvent) => any) {
        var b = document.createElement("button");
        b.className = "UIElement";
        b.style.position = "relative";
        b.textContent = text;
        b.onclick = callback.bind(this);
        b.style.width = "100%";
        this.ui.appendChild(b);
        this.ui.style.height = (this.ui.offsetHeight + b.offsetHeight + 5) + "px";
        this.closedPostion = this.openPosition - this.collapseButton.offsetTop;
    }
    public AddNumberInput(labelText:string) {
        var uiDiv = document.createElement("div");
        var label = document.createElement("label");
        var input = document.createElement("input");

        label.textContent = labelText;
        input.type = "number";
        uiDiv.className = input.className = "UIElement";
        uiDiv.appendChild(label);
        uiDiv.appendChild(input);
        
        this.ui.appendChild(uiDiv);
    }
    
    private time: number = 0;
    public OpenFrame() {
        this.ui.style.top = SilMath.Lerp(this.closedPostion, this.openPosition, this.time) + "px";
        if (this.time >= 1) {
            cancelAnimationFrame(this.animID);
            this.time = 0;
            this.open = true;
        } else {
            this.time += .2;
            this.animID = requestAnimationFrame(this.OpenFrame.bind(this));
        }
    }
    public CloseFrame() {
        this.ui.style.top = SilMath.Lerp(this.openPosition, this.closedPostion, this.time) + "px";
        
        if (this.time >= 1) {
            cancelAnimationFrame(this.animID);
            this.time = 0;
            this.open = false;
        } else {
            this.time += .2;
            this.animID = requestAnimationFrame(this.CloseFrame.bind(this));
        }
    }
    
    public OnClick(ev: MouseEvent) {
        if (this.open) {
            this.animID = requestAnimationFrame(this.CloseFrame.bind(this));
            this.collapseButton.textContent = "Open UI";
        } else {
            this.animID = requestAnimationFrame(this.OpenFrame.bind(this));
            this.collapseButton.textContent = "Close UI";
        }
    }
} 
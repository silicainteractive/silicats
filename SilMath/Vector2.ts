import SilicaTS = require("Matrix");


export class Vector2 {
    constructor(public x: number, public y: number) {
    }
    public Add(v: Vector2) {
        return new Vector2(this.x + v.x, this.y + v.y);
    }
    public Subtract(v: Vector2) {
        return new Vector2(this.x - v.x, this.y - v.y);
    }
    public MultiplyScalar(n: number) {
        return new Vector2(this.x * n, this.y * n);
    }
    public Rotate(theta: number) {
        var cosT = Math.cos(theta);
        var sinT = Math.sin(theta);

        var x = this.x * cosT - this.y * sinT;
        var y = this.x * sinT + this.y * cosT;
        return new Vector2(x, y);

    }
    public Length() {
        return (Math.sqrt(this.x * this.x + this.y * this.y));
    }
    public MultiplyMatrix(matrix: SilicaTS.Matrix2X2) {
        return new Vector2(this.x * matrix.values[0][0] - this.y * -matrix.values[0][1], this.x * matrix.values[1][0] + this.y * matrix.values[1][1]);
    }
}
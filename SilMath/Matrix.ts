export class Matrix2X2 {
    public values: Array<Array<number>>;

    constructor() {
        this.values = new Array<Array<number>>(2);
    }
    public static RotaionMatrix(theta: number): Matrix2X2 {
        var retval = new Matrix2X2();
        var m0 = Math.cos(theta);
        var m2 = Math.sin(theta);
        var m1 = -m2;
        retval.values[0] = new Array(m0, m1);
        retval.values[1] = new Array(m2, m0);

        return retval;
    }

} 
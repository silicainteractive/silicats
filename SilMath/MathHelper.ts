export function Lerp(a: number, b: number, t: number): number {
    return a + (b - a) * t;
}